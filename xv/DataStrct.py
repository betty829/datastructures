class Data:
    def __init__(self,data):
        self.data=data

    def method(self):
        print (f"i'm just a method {self.data}")

class Ordenador(Data):
# ordenamiento burbuja 
#    datos=[5,8,3,0,1]
    def ordenar(self):
        for numPasada in range(len(self.data)-1,0,-1):
            for i in range(numPasada):
                if self.data[i]>self.data[i+1]:
                    temp = self.data[i]
                    self.data[i] = self.data[i+1]
                    self.data[i+1] = temp
        return self.data

#ordenar=Ordenador(Ordenador.datos)
#print(ordenar.ordenar())

class Buscador(Ordenador):
   def buscar(self,valor):
       self.ordenar()
       izq=0
       der=len(self.data) -1
       while izq <= der:
           medio=(izq + der)// 2
           if self.data[medio] == valor:
               return medio
           elif self.data[medio] > valor:
               der = medio - 1
           else:
               izq = medio + 1
       return -1
#buscar=Buscador([5,4,3,2,1] )
#print(buscar.buscar(5))
#print(ordenar.ordenar())
#print(buscar(valor).buscar())        